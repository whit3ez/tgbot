from aiogram import types
from loader import dp

from utils.dog import get_random_dog_media


@dp.message_handler()
async def dog(message: types.Message):
    media_url = get_random_dog_media()
    if media_url.lower().endswith('mp4'):
        await message.answer_video(media_url)
    else:
        await message.answer_photo(media_url)

