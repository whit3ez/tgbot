import requests

def get_random_dog_media():

    url = "https://random.dog/woof.json"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        media_url = data.get("url")
        if media_url.lower().endswith(("jpg", "jpeg", "png", "gif", "mp4")):
            return media_url

    return None
